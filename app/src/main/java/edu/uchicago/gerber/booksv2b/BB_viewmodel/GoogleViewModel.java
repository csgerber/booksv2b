package edu.uchicago.gerber.booksv2b.BB_viewmodel;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import edu.uchicago.gerber.booksv2b.CC_model.models.GoogleResponse;
import edu.uchicago.gerber.booksv2b.CC_model.repo.BookRepository;


public class GoogleViewModel extends AndroidViewModel {
    private BookRepository bookRepository;
    private MutableLiveData<GoogleResponse> googleResponseMutableLiveData;

    //this is called from the Fragment in this line
    //viewModel = ViewModelProviders.of(this).get(BookSearchViewModel.class);
    public GoogleViewModel(@NonNull Application application) {
        super(application);
        googleResponseMutableLiveData = new MutableLiveData<>();
        bookRepository = new BookRepository(this);

    }

    //perform search
    public void  searchVolumes(String keyword) {
        bookRepository.searchVolumes(keyword);
    }

    //getter
    public MutableLiveData<GoogleResponse> getGoogleResponseMutableLiveData() {
        return googleResponseMutableLiveData;
    }


}