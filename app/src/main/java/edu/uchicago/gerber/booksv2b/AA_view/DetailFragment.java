package edu.uchicago.gerber.booksv2b.AA_view;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import edu.uchicago.gerber.booksv2b.R;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;




public class DetailFragment extends Fragment {


    private TextView txtAuthor;
    private TextView txtTitle;
    private TextView txtYear;
    private TextView txtDesc;
    private ImageView imgBook;

    private Button btnPrevious;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView = inflater.inflate(R.layout.fragment_detail, container, false);

        if (null == getArguments()) return containerView;

        //get refs to textViews
        txtAuthor = containerView.findViewById(R.id.txtAuthor);
        txtTitle = containerView.findViewById(R.id.txtTitle);
        txtYear = containerView.findViewById(R.id.txtYear);
        txtDesc = containerView.findViewById(R.id.txtDesc);

        //get ref to imageView
        imgBook = containerView.findViewById(R.id.imgBook);

        txtAuthor.setText(getArguments().getString(getString(R.string.auth)));
        txtTitle.setText(getArguments().getString(getString(R.string.title)));
        txtYear.setText(getArguments().getString(getString(R.string.year)));
        txtDesc.setText(getArguments().getString(getString(R.string.description)));

        //get the image-url
        String imageUrl = getArguments().getString(getString(R.string.imageUrlLarge));
        //use glide to insert the image into the imageView
        Glide.with(getContext())
                .load(imageUrl)
                .into(imgBook);



        btnPrevious = containerView.findViewById(R.id.btnPrevious);
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });

        return containerView;
    }
}