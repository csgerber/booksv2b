package edu.uchicago.gerber.booksv2b.CC_model.repo;


import edu.uchicago.gerber.booksv2b.BB_viewmodel.GoogleViewModel;
import edu.uchicago.gerber.booksv2b.CC_model.models.GoogleResponse;
import edu.uchicago.gerber.booksv2b.CC_model.service.BookService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

public class BookRepository {
    private static final String BOOK_SEARCH_SERVICE_BASE_URL = "https://www.googleapis.com/";

    //this is the max you can get
    private static final String MAX_RESULTS = "40";
    private final BookService bookSearchService;

    private GoogleViewModel googleViewModel;


    //the constructor creates a reference to the liveData object with callbacks
    public BookRepository(GoogleViewModel googleViewModel) {

        this.googleViewModel = googleViewModel;



        //used for intercepting outgoing and incoming traffic
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        //use retrofit to create this service
        bookSearchService = new retrofit2.Retrofit.Builder()
                .baseUrl(BOOK_SEARCH_SERVICE_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(BookService.class);

    }


    //this corresponds to the GET call from the api
    public void searchVolumes(String keyword) {
        bookSearchService.searchVolumes(keyword, MAX_RESULTS)
                .enqueue(new Callback<GoogleResponse>() {
                    @Override
                    public void onResponse(Call<GoogleResponse> call, Response<GoogleResponse> response) {
                        if (response.body() != null) {
                            googleViewModel.getGoogleResponseMutableLiveData().postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<GoogleResponse> call, Throwable t) {
                        googleViewModel.getGoogleResponseMutableLiveData().postValue(null);
                    }
                });
    }





}
